plugins {
    application
}


dependencies {
    implementation(project(":common"))
    implementation(project(":measurement"))
}
