package server;

import arch.ArchTypes;
import arch.ServerBase;
import com.google.protobuf.InvalidProtocolBufferException;
import measurement.ServerMeasurement;
import metrics.ServerMetrics;
import metrics.collector.MetricsCollector;
import proto.MessageArray;
import utils.ArrayUtils;
import utils.Constants;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;

public class AsyncServer implements ServerBase {
    MetricsCollector metricsCollector = new MetricsCollector();
    ServerAsyncContext serverAsyncContext = new ServerAsyncContext(metricsCollector);
    private AsynchronousServerSocketChannel serverSocketChannel;

    @Override
    public ArchTypes getArchType() {
        return ArchTypes.ASYNC;
    }

    @Override
    public void start(AtomicBoolean isRunning) throws InterruptedException, IOException {
        serverSocketChannel = AsynchronousServerSocketChannel.open().bind(new InetSocketAddress(Constants.SERVER_PORT), 200);
        while (isRunning.get()) {
            AsynchronousSocketChannel clientSocketChannel;
            try {
                clientSocketChannel = serverSocketChannel.accept().get();
            } catch (ExecutionException e) {
                if (!isRunning.get())
                    break;

                throw new RuntimeException("unexpected close");
            }
            ByteBuffer buffer = ByteBuffer.allocate(Constants.BUFFER_SIZE);
            clientSocketChannel.read(buffer, new Context(serverAsyncContext, new ReadingContext(), new WritingContext()), new CompletionHandler<Integer, Context>() {
                @Override
                public void completed(Integer result, Context context) {
                    if (result < 0) {
                        metricsCollector.stopCollecting();
                        return;
                    }
                    buffer.flip();
                    context.readingContext().completeMessage(buffer, result);
                    buffer.clear();
                    if (!context.readingContext().isFull()) {
                        clientSocketChannel.read(buffer, context, this);
                        return;
                    }

                    var clientTime = new ServerMeasurement();
                    metricsCollector.startMeasureTime(clientTime);

                    context.asyncContext().processMessage(
                            clientSocketChannel,
                            buffer,
                            context,
                            this,
                            clientTime
                    );
                }

                @Override
                public void failed(Throwable exc, Context context) {
                    metricsCollector.stopCollecting();
                    System.out.println(exc.getMessage());
                }
            });
        }
        serverAsyncContext.close();
    }

    @Override
    public void stop() throws IOException {
        serverSocketChannel.close();
    }

    @Override
    public void reset() {
        metricsCollector.resetMetrics();
    }

    @Override
    public ServerMetrics getMetrics() {
        return metricsCollector.calculateMetrics();
    }
}
