package server;

import measurement.ServerMeasurement;

public record Context(ServerAsyncContext asyncContext, ReadingContext readingContext, WritingContext writingContext) {
}
