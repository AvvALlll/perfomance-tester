package server;

import utils.Constants;

import java.nio.ByteBuffer;

import static java.lang.Integer.min;

public class WritingContext {
    private byte[] data;
    private Integer remainingSize = 0;

    public void reset(byte[] data) {
        this.data = data;
        remainingSize = data.length;
    }
    public void putFirstPart(ByteBuffer buffer) {
        buffer.putInt(data.length);
        buffer.put(data, 0, min(remainingSize, Constants.BUFFER_SIZE - Integer.BYTES));
        remainingSize = remainingSize - Constants.BUFFER_SIZE + Integer.BYTES;
    }

    public boolean isComplete() {
        return remainingSize <= 0;
    }

    public void putPartialData(ByteBuffer buffer) {
        buffer.put(data, data.length - remainingSize, min(remainingSize, Constants.BUFFER_SIZE));
        remainingSize -= Constants.BUFFER_SIZE;
    }
}
