package server;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import measurement.ServerMeasurement;
import metrics.ServerMetrics;
import metrics.collector.MetricsCollector;
import proto.MessageArray;
import utils.ArrayUtils;
import utils.Constants;
import utils.Sorting;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static java.lang.Integer.min;

public class ServerAsyncContext {
    MetricsCollector metricsCollector;
    ThreadPoolExecutor threadPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(Constants.THREAD_POOL_SIZE);

    ServerAsyncContext(MetricsCollector metricsCollector) {
        this.metricsCollector = metricsCollector;
    }

    void processMessage(AsynchronousSocketChannel clientSocketChannel, ByteBuffer buffer, Context context, CompletionHandler<Integer, Context> callback, ServerMeasurement clientTime) {
        threadPool.submit(() -> {
            //start client processing measurement
            var array = convertToArrayList(context);

            var requestTime = new ServerMeasurement();
            metricsCollector.startMeasureTime(requestTime);
            Sorting.sort(array);
            metricsCollector.collectTotalRequestHandlingTime(requestTime);

            byte[] outData = MessageArray.newBuilder().setArraySize(array.size()).addAllData(array).build().toByteArray();

            metricsCollector.collectTotalClientHandlingTime(clientTime);

            context.writingContext().reset(outData);
            buffer.clear();
            context.writingContext().putFirstPart(buffer);
            buffer.flip();

            clientSocketChannel.write(buffer, context, new CompletionHandler<Integer, Context>() {
                @Override
                public void completed(Integer result, Context attachment) {
                    if (buffer.hasRemaining()) {
                        clientSocketChannel.write(buffer, context, this);
                        return;
                    }

                    buffer.clear();
                    if (!context.writingContext().isComplete()) {
                        context.writingContext().putPartialData(buffer);
                        buffer.flip();
                        clientSocketChannel.write(buffer, context, this);
                    } else {
                        //finish client processing measurement

                        buffer.clear();
                        context.readingContext().reset();
                        clientSocketChannel.read(buffer, context, callback);
                    }
                }

                @Override
                public void failed(Throwable exc, Context attachment) {
                    System.out.println(exc.getMessage());
                }
            });
        });
    }

    public void close() {
        threadPool.shutdown();
    }

    private ArrayList<Integer> convertToArrayList(Context context) {
        MessageArray messageArray = null;
        try {
            messageArray = MessageArray.parseFrom(context.readingContext().data);
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e);
        }
        return new ArrayList<Integer>(messageArray.getDataList());
    }
}
