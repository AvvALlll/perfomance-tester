package server;

import java.nio.ByteBuffer;
import java.util.Objects;

public class ReadingContext {
    private Integer messageSize = null;
    private Integer curReadSize = 0;
    public byte[] data = new byte[0];

    public boolean isFull() {
        return Objects.equals(messageSize, curReadSize);
    }

    public void reset() {
        messageSize = null;
        curReadSize = 0;
    }

    public void completeMessage(ByteBuffer buffer, Integer readSize) {
        if (messageSize == null) {
            messageSize = buffer.getInt();
            readSize -= Integer.BYTES;
            data = new byte[messageSize];
        }
        buffer.get(data, curReadSize, readSize);
        curReadSize += readSize;
    }
}
