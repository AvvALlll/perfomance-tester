package cli;

import arch.ServerBase;
import client.parameters.ClientParameterBase;
import parameters.Parameters;

import java.util.HashMap;
import java.util.List;

public class TesterArguments {
    public ServerBase server;
    HashMap<Parameters, ClientParameterBase> args = new HashMap<>();

    public TesterArguments(ServerBase server) {
        this.server = server;
    }

    public void setArgs(Parameters paramName, ClientParameterBase parameter) {
        args.put(paramName, parameter);
    }

    public ClientParameterBase getArg(Parameters paramName) {
        return args.get(paramName);
    }

    public List<ClientParameterBase> getClientParameters() {
        return args.values().stream().toList();
    }
}
