package cli;

import arch.ServerBase;
import client.parameters.ClientParameter;
import client.parameters.ClientParameterRange;
import org.apache.commons.cli.*;
import parameters.Parameters;
import server.AsyncServer;
import server.SyncBlockServer;
import server.SyncNonBlockServer;

public class TesterCLI {
    private static Options getOptions() {
        Options options = new Options();
        options.addOption(
                Option.builder("arch")
                        .argName("archName")
                        .hasArg()
                        .required()
                        .desc("server architecture")
                        .build()
        );
        options.addOption(
                Option.builder("X")
                        .hasArg()
                        .desc("total number of requests")
                        .required()
                        .build()
        );
        options.addOption(
                Option.builder("p1")
                        .hasArgs()
                        .valueSeparator('=')
                        .required()
                        .numberOfArgs(2)
                        .build()
        );
        options.addOption(
                Option.builder("p2")
                        .hasArgs()
                        .valueSeparator('=')
                        .required()
                        .numberOfArgs(2)
                        .build()
        );
        options.addOption(
                Option.builder("target")
                        .hasArgs()
                        .required()
                        .numberOfArgs(4)
                        .build()
        );
        return options;
    }

    public static TesterArguments getTesterArguments(String args[]) throws IllegalArgumentException, ParseException {
        CommandLineParser parser = new DefaultParser();
        CommandLine line = parser.parse(TesterCLI.getOptions(), args);
        var testerArgs = new TesterArguments(
                getServerArch(line));
        getRequestCount(line, testerArgs);
        getUnchangedParameter(line, "p1", testerArgs);
        getUnchangedParameter(line, "p2", testerArgs);
        getChangedParameter(line, testerArgs);
        return testerArgs;
    }

    private static ServerBase getServerArch(CommandLine line) throws IllegalArgumentException {
        return switch (line.getOptionValue("arch")) {
            case "async" -> new AsyncServer();
            case "syncBlock" -> new SyncBlockServer();
            case "syncNonBlock" -> new SyncNonBlockServer();
            default -> throw new IllegalArgumentException("server architecture not recognized");
        };
    }

    private static void getRequestCount(CommandLine line, TesterArguments testerArgs) {
        testerArgs.setArgs(Parameters.REQUEST_COUNT,
                new ClientParameter(Parameters.REQUEST_COUNT, Integer.parseInt(line.getOptionValue("X"))));
    }

    private static void getUnchangedParameter(CommandLine line, String optionName, TesterArguments testerArgs) {
        var optArgs = line.getOptionValues(optionName);
        var parName = Parameters.fromString(optArgs[0]);
        testerArgs.setArgs(parName, new ClientParameter(parName, Integer.parseInt(optArgs[1])));
    }

    private static void getChangedParameter(CommandLine line, TesterArguments testerArgs) {
        var targetArgs = line.getOptionValues("target");
        var nameArg = targetArgs[0].split("=");
        if (!nameArg[0].equals("name"))
            throw new IllegalStateException("Unexpected name arg: " + nameArg[0]);
        var parName = Parameters.fromString(nameArg[1]);

        var fromArg = targetArgs[1].split("=");
        if (!fromArg[0].equals("from"))
            throw new IllegalStateException("Unexpected from arg: " + fromArg[0]);
        var parFrom = Integer.parseInt(fromArg[1]);

        var toArg = targetArgs[2].split("=");
        if (!toArg[0].equals("to"))
            throw new IllegalStateException("Unexpected to arg: " + toArg[0]);
        var parTo = Integer.parseInt(toArg[1]);

        if (parFrom > parTo)
            throw new RuntimeException("invalid range for target arg");

        var stepArg = targetArgs[3].split("=");
        if (!stepArg[0].equals("step"))
            throw new IllegalStateException("Unexpected step arg: " + stepArg[0]);
        var parStep = Integer.parseInt(stepArg[1]);
        testerArgs.setArgs(parName, new ClientParameterRange(parName, parFrom, parTo, parStep));
    }
}
