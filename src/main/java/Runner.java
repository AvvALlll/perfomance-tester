import cli.TesterCLI;
import manager.RunManager;
import manager.client.ClientManager;
import manager.server.ServerManager;
import org.apache.commons.cli.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;

public class Runner {
    static public void main(String[] args) throws ParseException, IOException, InterruptedException {
        var testerArgs = TesterCLI.getTesterArguments(args);
        RunManager manager = new RunManager(testerArgs);
//        try {
            manager.start();
//        } catch (Exception e) {
//            System.err.println(e.getMessage());
//        }
    }
}
