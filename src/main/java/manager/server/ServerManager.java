package manager.server;

import arch.ArchTypes;
import arch.ServerBase;
import cli.TesterArguments;
import metrics.ServerMetrics;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;

public class ServerManager {
    private final ServerBase server;
    private Thread serverThread;
    public ServerManager(ServerBase server) {
        this.server = server;
    }

    public void start(AtomicBoolean isRunning) throws InterruptedException {
        serverThread = new Thread(() -> {
            try {
                server.start(isRunning);
            } catch (ExecutionException | InterruptedException | IOException e) {
                throw new RuntimeException(e);
            }
        });
        serverThread.start();
    }

    public void stop() throws IOException, InterruptedException {
        server.stop();
        serverThread.join();
    }

    public void resetServer() {
        server.reset();
    }
    public ServerMetrics getMetrics() {
        return server.getMetrics();
    }

    public ArchTypes getArchType() { return server.getArchType(); }
}
