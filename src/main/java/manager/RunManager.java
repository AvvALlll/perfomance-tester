package manager;

import cli.TesterArguments;
import client.parameters.ClientParameter;
import client.parameters.ClientParameterRange;
import graphic.GraphicCreator;
import manager.client.ClientManager;
import manager.server.ServerManager;
import metrics.*;
import parameters.Parameters;
import statistic.StatisticIO;
import statistic.StatisticInfo;
import utils.Constants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class RunManager {
    private final ClientManager clientManager = new ClientManager();
    private final TesterArguments testerArgs;
    private final ServerManager serverManager;

    public RunManager(TesterArguments testerArgs) {
        this.testerArgs = testerArgs;
        this.serverManager = new ServerManager(testerArgs.server);
    }

    public void start() throws InterruptedException, IOException {
        var clientParameters = testerArgs.getClientParameters();
        var defaultParameters = clientParameters.stream().filter((param) -> !(param instanceof ClientParameterRange)).map((param) -> (ClientParameter) param).toList();
        var rangeParameter = (ClientParameterRange) clientParameters.stream().filter((param) -> param instanceof ClientParameterRange).toList().getFirst();

        AtomicBoolean isRunning = new AtomicBoolean(true);
        ArrayList<Integer> targetParameterValues = new ArrayList<>();
        RequestAvg requestAvg = new RequestAvg();
        RequestServerAvg requestServerAvg = new RequestServerAvg();
        ClientProcessingServer clientProcessingServer = new ClientProcessingServer();

        serverManager.start(isRunning);
        for (int paramValue = rangeParameter.valueFrom(), skipValue = 0; paramValue <= rangeParameter.valueTo(); paramValue += rangeParameter.step(), skipValue++) {
            var parameters = new ArrayList<>(defaultParameters);
            Integer clientCount = rangeParameter.parameterName() == Parameters.CLIENTS_COUNT ? paramValue :
                    ((ClientParameter) testerArgs.getArg(Parameters.CLIENTS_COUNT)).parameterValue();

            var targetParameter = new ClientParameter(rangeParameter.parameterName(), paramValue);
            parameters.add(targetParameter);

            var clientMetric = clientManager.runClients(clientCount, parameters);
            var serverMetrics = serverManager.getMetrics();

            if (skipValue < Constants.SKIP_RANGE)
                continue;
            else if (skipValue == Constants.SKIP_RANGE) {
                paramValue = rangeParameter.valueFrom();
                continue;
            }
            targetParameterValues.add(paramValue);
            requestAvg.addValue(clientMetric.averageRequestTime());
            requestServerAvg.addValue(serverMetrics.averageRequestTime());
            clientProcessingServer.addValue(serverMetrics.averageClientTime());

            serverManager.resetServer();
            System.gc();
        }
        StatisticInfo statInfo = new StatisticInfo(
                new TargetParameter(rangeParameter.parameterName().toString(), targetParameterValues),
                serverManager.getArchType().toString(),
                defaultParameters,
                List.of(requestAvg, requestServerAvg, clientProcessingServer));
        StatisticIO.save(statInfo);
        GraphicCreator.createGraphics(statInfo);

        isRunning.set(false);
        serverManager.stop();
    }
}
