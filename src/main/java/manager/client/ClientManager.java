package manager.client;

import client.Client;
import client.ClientMemContext;
import client.parameters.ClientParameter;
import metrics.ClientMetric;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

public class ClientManager {
    List<ClientMemContext> clientMemContexts = new ArrayList<>();

    public ClientMetric runClients(Integer clientCount, List<ClientParameter> clientParameters) throws IOException {
        if (clientMemContexts.size() < clientCount) {
            for (int i = clientMemContexts.size(); i < clientCount; ++i) {
                clientMemContexts.add(new ClientMemContext());
            }
        }
        ArrayList<Thread> clientThreads = new ArrayList<>();
        AtomicReference<Double> totalRequestTime = new AtomicReference<>(0.0);
        CountDownLatch countDownLatch = new CountDownLatch(clientCount);
        for (int i = 0; i < clientCount; ++i) {

            int finalI = i;
            var clientThread = new Thread(() -> {
                try {
                    var client = new Client(clientMemContexts.get(finalI));
                    client.connect();
                    countDownLatch.countDown();
                    countDownLatch.await();
                    client.start(clientParameters);

                    totalRequestTime.updateAndGet(v -> v + client.getAverageRequest());
                } catch (IOException | ClassNotFoundException | InterruptedException e) {
                    throw new RuntimeException(e);
                }
            });
            clientThreads.add(clientThread);
            clientThread.start();
        }

        clientThreads.forEach((client) -> {
            try {
                client.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        return new ClientMetric(totalRequestTime.get() / clientCount);
    }
}
