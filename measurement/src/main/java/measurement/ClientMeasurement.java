package measurement;

public class ClientMeasurement {
    Long startTime = 0L;
    Long totalTime = 0L;
    Integer numberMeasurements = 0;

    public void startMeasure() {
        numberMeasurements++;
        startTime = System.currentTimeMillis();
    }

    public void stopMeasure() {
        totalTime += (System.currentTimeMillis() - startTime);
    }

    public Double getAverage() {
        return totalTime.doubleValue() / numberMeasurements;
    }
}
