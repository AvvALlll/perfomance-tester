package measurement;

import java.util.concurrent.atomic.AtomicLong;

public class ServerMeasurement {
    public Long startTime = 0L;

    public void startMeasure() {
        startTime = System.currentTimeMillis();
    }

    public Long stopMeasure() {
        return System.currentTimeMillis() - startTime;
    }

}
