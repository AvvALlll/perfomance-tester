package metrics.collector;

import measurement.ServerMeasurement;
import metrics.ServerMetrics;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class MetricsCollectorAvg extends MetricsCollectorBase {
    AtomicLong totalRequests = new AtomicLong(0);
    AtomicLong totalClients = new AtomicLong(0);
    AtomicLong totalRequestHandlingTime = new AtomicLong(0);
    AtomicLong totalClientHandlingTime = new AtomicLong(0);

    @Override
    public void resetMetrics() {
        isCollecting.set(true);
        totalRequests.set(0);
        totalClients.set(0);
        totalRequestHandlingTime.set(0);
        totalClientHandlingTime.set(0);
    }

    @Override
    public void collectTotalRequestHandlingTime(ServerMeasurement serverMeasurement) {
        if (isCollecting.get()) {
            totalRequestHandlingTime.addAndGet(serverMeasurement.stopMeasure());
            totalRequests.incrementAndGet();
        }
    }

    @Override
    public void collectTotalClientHandlingTime(ServerMeasurement serverMeasurement) {
        if (isCollecting.get()) {
            totalClientHandlingTime.addAndGet(serverMeasurement.stopMeasure());
            totalClients.incrementAndGet();
        }
    }

    @Override
    public ServerMetrics calculateMetrics() {
        return new ServerMetrics(totalRequestHandlingTime.doubleValue() / totalRequests.get(),
                totalClientHandlingTime.doubleValue() / totalClients.get());
    }
}
