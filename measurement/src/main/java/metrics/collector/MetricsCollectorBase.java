package metrics.collector;

import measurement.ServerMeasurement;
import metrics.ServerMetrics;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class  MetricsCollectorBase {
    AtomicBoolean isCollecting = new AtomicBoolean(true);
    public void stopCollecting() {
        isCollecting.set(false);
    }
    public void startMeasureTime(ServerMeasurement serverMeasurement) {
        if (isCollecting.get())
            serverMeasurement.startMeasure();
    }

    public abstract void resetMetrics();

    public abstract void collectTotalRequestHandlingTime(ServerMeasurement serverMeasurement);

    public abstract void collectTotalClientHandlingTime(ServerMeasurement serverMeasurement);

    public abstract ServerMetrics calculateMetrics();
}
