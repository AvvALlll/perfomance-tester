package metrics.collector;

import measurement.ServerMeasurement;
import metrics.ServerMetrics;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class MetricsCollector extends MetricsCollectorBase {
    Queue<Long> totalRequestHandlingTime = new ConcurrentLinkedQueue<>();
    Queue<Long> totalClientHandlingTime = new ConcurrentLinkedQueue<>();

    public void collectTotalRequestHandlingTime(ServerMeasurement serverMeasurement) {
        if (!isCollecting.get())
            return;

        var elapsedTime = serverMeasurement.stopMeasure();
        totalRequestHandlingTime.add(elapsedTime);
    }

    public void collectTotalClientHandlingTime(ServerMeasurement serverMeasurement) {
        if (!isCollecting.get())
            return;
        var elapsedTime = serverMeasurement.stopMeasure();
        totalClientHandlingTime.add(elapsedTime);
    }

    public ServerMetrics calculateMetrics() {
        var totalRequestHandlingTimeSorted = totalRequestHandlingTime.stream().sorted().toList();
        var totalRequestSize = totalRequestHandlingTimeSorted.size();
        Double percent = 0.2;
        Integer requestLeftBorder = (int) (totalRequestSize * percent), requestRightBorder = totalRequestSize - requestLeftBorder;

        var requestSum = IntStream.range(0, totalRequestSize).filter(i -> requestLeftBorder <= i && i <= requestRightBorder).mapToObj(totalRequestHandlingTimeSorted::get).reduce(0L, Long::sum);
//        var requestSum = newTotalRequestTime.stream().reduce(0L, Long::sum);

        var totalClientHandlingTimeSorted = totalClientHandlingTime.stream().sorted().toList();
        var totalClientSize = totalClientHandlingTimeSorted.size();
        Integer clientLeftBorder = (int) (totalClientSize * percent), clientRightBorder = totalRequestSize - clientLeftBorder;

        var clientSum = IntStream.range(0, totalClientSize).filter(i -> clientLeftBorder < i && i < clientRightBorder).mapToObj(totalClientHandlingTimeSorted::get).reduce(0l, Long::sum);
//        var clientSum = newTotalRequestTime.stream().reduce(0L, Long::sum);

        return new ServerMetrics((double) (requestSum / max(1, 2L * requestLeftBorder)), (double) (clientSum / max(1, 2L * clientLeftBorder)));
    }

    public void resetMetrics() {
        totalRequestHandlingTime.clear();
        totalClientHandlingTime.clear();
        isCollecting.set(true);
    }
}
