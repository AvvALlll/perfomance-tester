package metrics;

public record ServerMetrics(Double averageRequestTime, Double averageClientTime) {
}
