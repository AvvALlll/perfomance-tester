package metrics;

public record ClientMetric(Double averageRequestTime) {
}
