package server;

import arch.ArchTypes;
import arch.ServerBase;
import com.google.protobuf.InvalidProtocolBufferException;
import measurement.ServerMeasurement;
import metrics.collector.MetricsCollector;
import metrics.collector.MetricsCollectorAvg;
import metrics.collector.MetricsCollectorBase;
import proto.MessageArray;
import utils.Constants;
import utils.Sorting;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.lang.Math.min;

public class SyncNonBlockServer implements ServerBase {

    MetricsCollectorBase metricsCollector = new MetricsCollector();
    ServerSocketChannel serverSocketChannel;
    Selector readSelector;
    Selector writeSelector;

    Queue<ClientSyncNonBlockContext> readClientQueue = new ConcurrentLinkedQueue<>();
    Queue<ClientSyncNonBlockContext> writeClientQueue = new ConcurrentLinkedQueue<>();

    ThreadPoolExecutor threadPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(Constants.THREAD_POOL_SIZE);


    @Override
    public ArchTypes getArchType() {
        return ArchTypes.SYNC_NONBLOCK;
    }

    @Override
    public void start(AtomicBoolean isRunning) throws ExecutionException, InterruptedException, IOException {
        serverSocketChannel = ServerSocketChannel.open().bind(new InetSocketAddress(Constants.SERVER_PORT), 200);
        readSelector = Selector.open();
        writeSelector = Selector.open();

        var readThread = readProcessing(isRunning);
        var writeThread = writeProcessing(isRunning);

        while (isRunning.get()) {
            try {
                var clientSocket = serverSocketChannel.accept();
                clientSocket.configureBlocking(false);
                readClientQueue.add(new ClientSyncNonBlockContext(clientSocket));
                readSelector.wakeup();
            } catch (AsynchronousCloseException e) {
                if (!isRunning.get())
                    break;
                throw new RuntimeException("unexpected server socket close");
            }
        }
        readThread.join();
        writeThread.join();
        threadPool.shutdown();
        writeSelector.close();
        readSelector.close();
    }

    @Override
    public void stop() throws IOException {
        serverSocketChannel.close();
        readSelector.wakeup();
        writeSelector.wakeup();
    }

    @Override
    public void reset() {
        metricsCollector.resetMetrics();
    }

    @Override
    public metrics.ServerMetrics getMetrics() {
        return metricsCollector.calculateMetrics();
    }

    private Thread readProcessing(AtomicBoolean isRunning) {
        var readThread = new Thread(() -> {
            while (isRunning.get()) {
                int selectedChannels = 0;
                try {
                    selectedChannels = readSelector.select();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

                if (selectedChannels > 0) {
                    Set<SelectionKey> selectedKeys = readSelector.selectedKeys();
                    Iterator<SelectionKey> iter = selectedKeys.iterator();

                    while (iter.hasNext()) {
                        var selectedKey = iter.next();
                        var client = (ClientSyncNonBlockContext) selectedKey.attachment();
                        try {
                            if (!client.readMessage()) {
                                metricsCollector.stopCollecting();
                                selectedKey.cancel();
                                iter.remove();
                                continue;
                            }

                            if (client.isFullRead()) {
                                client.resetReadInfo();
                                var clientTime = new ServerMeasurement();
                                metricsCollector.startMeasureTime(clientTime);
                                submitTask(client, clientTime);
                            }
                        } catch (IOException e) {
                            metricsCollector.stopCollecting();
                            throw new RuntimeException(e);
                        }
                        iter.remove();
                    }
                }

                while (!readClientQueue.isEmpty()) {
                    var newClient = readClientQueue.poll();
                    try {
                        if (newClient.socketChannel.isConnected())
                            newClient.socketChannel.register(readSelector, SelectionKey.OP_READ, newClient);
                    } catch (ClosedChannelException e) {
                        metricsCollector.stopCollecting();
                        throw new RuntimeException(e);
                    }
                }
            }
        });
        readThread.start();
        return readThread;
    }

    private Thread writeProcessing(AtomicBoolean isRunning) throws IOException {
        var writeThread = new Thread( () -> {
            while (isRunning.get()) {
                int selectedChannels = 0;
                try {
                    selectedChannels = writeSelector.select();
                } catch (IOException e) {
                    metricsCollector.stopCollecting();
                    throw new RuntimeException(e);
                }
                
                while (!writeClientQueue.isEmpty()) {
                    var newClient = writeClientQueue.poll();
                    try {
                        newClient.socketChannel.register(writeSelector, SelectionKey.OP_WRITE, newClient);
                    } catch (CancelledKeyException | ClosedChannelException e) {
                        metricsCollector.stopCollecting();
                        throw new RuntimeException(e);
                    }
                }

                if (selectedChannels > 0) {
                    Set<SelectionKey> selectedKeys = writeSelector.selectedKeys();
                    Iterator<SelectionKey> iter = selectedKeys.iterator();

                    while (iter.hasNext()) {
                        var selectedKey = iter.next();
                        var client = (ClientSyncNonBlockContext) selectedKey.attachment();

                        try {
                            if (!client.writeMessage()) {
                                client.resetWriteInfo();
                                selectedKey.cancel();
                            }
                        } catch (IOException e) {
                            metricsCollector.stopCollecting();
                            throw new RuntimeException(e);
                        }
                        iter.remove();
                    }
                }

            }
        });
        writeThread.start();
        return writeThread;
    }

    private void submitTask(ClientSyncNonBlockContext clientSyncNonBlockContext, ServerMeasurement clientTime) {
        threadPool.submit(() -> {
            ArrayList<Integer> array;
            try {
                array = new ArrayList<Integer>(MessageArray.parseFrom(clientSyncNonBlockContext.byteArray).getDataList());
            } catch (InvalidProtocolBufferException e) {
                metricsCollector.stopCollecting();
                throw new RuntimeException(e);
            }

            var requestTime = new ServerMeasurement();
            metricsCollector.startMeasureTime(requestTime);
            Sorting.sort(array);
            metricsCollector.collectTotalRequestHandlingTime(requestTime);

            clientSyncNonBlockContext.outArray = MessageArray.newBuilder().setArraySize(array.size()).addAllData(array).build().toByteArray();
            metricsCollector.collectTotalClientHandlingTime(clientTime);

            writeClientQueue.add(clientSyncNonBlockContext);
            writeSelector.wakeup();

        });
    }
}
