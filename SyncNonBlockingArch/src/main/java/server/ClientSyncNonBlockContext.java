package server;

import utils.Constants;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import static java.lang.Math.min;

public class ClientSyncNonBlockContext {
    ByteBuffer readBuffer = ByteBuffer.allocate(Constants.BUFFER_SIZE);
    ByteBuffer writeBuffer = ByteBuffer.allocate(Constants.BUFFER_SIZE);
    SocketChannel socketChannel;

    int totalMessageSize = -1;
    int curMessageSize = -1;
    Integer curWriteMessageSize = null;
    byte[] byteArray = new byte[0];

    public byte[] outArray = new byte[0];

    public ClientSyncNonBlockContext(SocketChannel socketChannel) {
        this.socketChannel = socketChannel;
    }

    void resetReadInfo() {
        totalMessageSize = -1;
        curMessageSize = -1;
        readBuffer.clear();
    }

    void resetWriteInfo() {
        curWriteMessageSize = null;
        writeBuffer.clear();
    }

    boolean readMessage() throws IOException {
        int readSize = socketChannel.read(readBuffer);
        if (readSize < 0)
            return false;

        if (totalMessageSize == -1) {
            if (readSize < Integer.BYTES)
                return true;

            readSize -= Integer.BYTES;
            readBuffer.flip();
            totalMessageSize = readBuffer.getInt();
            curMessageSize = totalMessageSize;
            readBuffer.compact();
            byteArray = new byte[totalMessageSize];
        }
        readBuffer.flip();
        readBuffer.get(byteArray, totalMessageSize - curMessageSize, readSize);
        curMessageSize -= readSize;
        readBuffer.clear();

        return true;
    }

    boolean isFullRead() {
        return curMessageSize == 0;
    }

    boolean writeMessage() throws IOException {
        writeBuffer.flip();
        if (writeBuffer.hasRemaining()) {
            socketChannel.write(writeBuffer);
            return true;
        }

        writeBuffer.clear();
        Integer intSize = 0;
        if (curWriteMessageSize == null) {
            curWriteMessageSize = outArray.length;
            writeBuffer.putInt(curWriteMessageSize);
            intSize = Integer.BYTES;
        }

        if (curWriteMessageSize > 0) {
            writeBuffer.put(outArray, outArray.length - curWriteMessageSize, min(curWriteMessageSize, Constants.BUFFER_SIZE - intSize));
            curWriteMessageSize = curWriteMessageSize - Constants.BUFFER_SIZE + intSize;

            writeBuffer.flip();
            socketChannel.write(writeBuffer);
            writeBuffer.compact();
            return true;
        }

        return false;
    }
}
