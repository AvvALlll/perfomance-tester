plugins {
    application
}

group = "org.itmo.mse"
version = "1.0"

application {
    mainClass = "merger.GraphicMerger"
}


dependencies {
    implementation("commons-cli:commons-cli:1.6.0")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.14.0-rc1")
    implementation("org.jfree:jfreechart:1.5.0")
    implementation(project(":ClientArch"))
    implementation(project(":measurement"))
    implementation(project(":common"))
}

tasks.jar {
    manifest.attributes["Main-Class"] = application.mainClass
    val dependencies = configurations
        .runtimeClasspath
        .get()
        .map(::zipTree)
    from(dependencies)
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}
