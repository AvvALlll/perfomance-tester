package merger;

import graphic.GraphicCreator;
import metrics.Metric;
import org.jfree.data.xy.XYSeriesCollection;
import statistic.StatisticIO;

import java.io.IOException;
import java.util.stream.Collectors;

public class UnionGraphicCreator {
    static public void createUnionGraphic(MergerCLI.StatFiles statFiles) {
        var statInfoList = statFiles.fileList().stream().map(file -> {
            try {
                return StatisticIO.load(file);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }).toList();

        //validation
        for (int i = 0; i < statInfoList.size(); ++i) {
            var statInfo1 = statInfoList.get(i);
            for (int j = 0; j < statInfoList.size(); ++j) {
                var statInfo2 = statInfoList.get(j);
                if (statInfo1.targetParameter().equals(statInfo2.targetParameter().targetName()))
                    throw new RuntimeException("different input parameters, diagrams cannot be built");
            }
        }

        var firstStat = statInfoList.getFirst();
        var datasetMap = firstStat.metrics().stream().collect(Collectors.toMap(Metric::getMetricName, it -> new XYSeriesCollection()));

        statInfoList.stream().forEach(it ->
                it.metrics().forEach(
                        metric -> {
                            datasetMap.get(metric.getMetricName()).addSeries(GraphicCreator.createSeries(it.archType(), it.targetParameter(), metric.metricValues));
                        }
                )
        );

        firstStat.metrics().forEach( metric ->
                {
                    try {
                        metric.createGraphic(firstStat.archType(), firstStat.targetParameter().targetName(), datasetMap.get(metric.getMetricName()), true);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
        );
    }
}
