package merger;

import org.apache.commons.cli.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class MergerCLI {
    public record StatFiles(List<File> fileList) {}
    private static Options getOptions() {
        Options options = new Options();
        options.addOption(Option.builder("f1").hasArgs().required().desc("filename with first arch").build());
        options.addOption(Option.builder("f2").hasArgs().required().desc("filename with second arch").build());
        options.addOption(Option.builder("f3").hasArgs().required().desc("filename with third arch").build());
        return options;
    }

    static public StatFiles getStatFiles(String[] args) throws ParseException, FileNotFoundException {
        CommandLineParser parser = new DefaultParser();
        CommandLine line = parser.parse(MergerCLI.getOptions(), args);

        ArrayList<File> fileList = new ArrayList<>();
        var filenameF1 = line.getOptionValue("f1");
        var f1 = new File(filenameF1);
        if (!f1.exists()) {
            throw new FileNotFoundException(filenameF1 + " doesn't exist");
        }
        fileList.add(f1);

        var filenameF2 = line.getOptionValue("f2");
        var f2 = new File(filenameF2);
        if (!f2.exists()) {
            throw new FileNotFoundException(filenameF2 + " doesn't exist");
        }
        fileList.add(f2);

        var filenameF3 = line.getOptionValue("f3");
        var f3 = new File(filenameF3);
        if (!f3.exists()) {
            throw new FileNotFoundException(filenameF3 + " doesn't exist");
        }
        fileList.add(f3);

        return new StatFiles(fileList);
    }


}
