package merger;

public class GraphicMerger {
    static public void main(String[] args) {
        try {
            UnionGraphicCreator.createUnionGraphic(MergerCLI.getStatFiles(args));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
