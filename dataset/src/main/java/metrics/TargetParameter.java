package metrics;

import java.util.ArrayList;
import java.util.List;

public record TargetParameter(String targetName,List<Integer> values) {
}
