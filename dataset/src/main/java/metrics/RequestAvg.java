package metrics;

import org.jfree.chart.ChartUtils;
import org.jfree.data.xy.XYDataset;
import utils.Constants;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class RequestAvg extends Metric {
    public RequestAvg() {}

    public RequestAvg(List<Double> values) {
        super(values);
    }
    @Override
    public void createGraphic(String archType, String targetName, XYDataset dataset, boolean isUnion) throws IOException {
        var chart = createChart("Average request time", targetName, dataset);
        File file = isUnion ?
                Constants.graphicPath.resolve(targetName + "_requestAvg_union.png").toFile()
                : Constants.graphicPath.resolve(archType + "_" + targetName + "_requestAvg.png").toFile();
        ChartUtils.saveChartAsPNG(file, chart, 700, 600);
    }

    @Override
    public String getMetricName() {
        return "requestAvg";
    }
}
