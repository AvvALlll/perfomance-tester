package metrics.factory;

import metrics.ClientProcessingServer;
import metrics.Metric;
import metrics.RequestAvg;
import metrics.RequestServerAvg;

import java.util.List;

public class MetricFactory {
    public static Metric getMetric(String metricName, List<Double> values) {
        return switch (metricName) {
            case "client processing avg" -> new ClientProcessingServer(values);
            case "requestAvg" -> new RequestAvg(values);
            case "requestServerAvg" -> new RequestServerAvg(values);
            default -> throw new RuntimeException("cannot recognize metric");
        };
    }
}
