package metrics;

import org.jfree.chart.ChartUtils;
import org.jfree.data.xy.XYDataset;
import utils.Constants;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ClientProcessingServer extends Metric {
    public ClientProcessingServer() {
    }

    public ClientProcessingServer(List<Double> values) {
        super(values);
    }

    @Override
    public void createGraphic(String archType, String targetName, XYDataset dataset, boolean isUnion) throws IOException {
        var chart = createChart("Average client processing time", targetName, dataset);
        File file = isUnion ?
            Constants.graphicPath.resolve(targetName + "_clientProcessingServer_union.png").toFile()
            : Constants.graphicPath.resolve(archType + "_" + targetName + "_clientProcessingServer.png").toFile();

        ChartUtils.saveChartAsPNG(file, chart, 700, 600);
    }

    @Override
    public String getMetricName() {
        return "client processing avg";
    }
}
