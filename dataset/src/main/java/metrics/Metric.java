package metrics;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYDataset;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class Metric {
    public final List<Double> metricValues = new ArrayList<>();
    public Metric() {}
    public Metric(List<Double> values) {
        metricValues.addAll(values);
    }
    public void addValue(Double value) {
        metricValues.add(value);
    }

    public abstract void createGraphic(String archType, String targetName, XYDataset series, boolean isUnion) throws IOException;

    public abstract String getMetricName();

    protected JFreeChart createChart(String titleName, String targetName, XYDataset dataset) {
        JFreeChart chart = ChartFactory.createXYLineChart(
                titleName + ", ms",
                targetName + ", ms",
                getMetricName(),
                dataset,
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );
        XYPlot plot = chart.getXYPlot();
        plot.setBackgroundPaint(Color.white);

        plot.setRangeGridlinesVisible(true);
        plot.setRangeGridlinePaint(Color.BLACK);

        plot.setDomainGridlinesVisible(true);
        plot.setDomainGridlinePaint(Color.BLACK);

        chart.getLegend().setFrame(BlockBorder.NONE);

        chart.setTitle(new TextTitle(titleName,
                        new Font("Serif", java.awt.Font.BOLD, 24)
                )
        );
        return chart;
    }
}
