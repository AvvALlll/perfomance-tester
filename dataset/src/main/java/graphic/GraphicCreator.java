package graphic;

import metrics.TargetParameter;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import statistic.StatisticInfo;

import java.io.IOException;
import java.util.List;
import java.util.stream.IntStream;

public class GraphicCreator {


    static public void createGraphics(StatisticInfo outStatistics) {
        outStatistics.metrics().forEach( it -> {
            try {
                var dataset = new XYSeriesCollection();
                dataset.addSeries(createSeries(outStatistics.archType(), outStatistics.targetParameter(), it.metricValues));
                it.createGraphic(outStatistics.archType(), outStatistics.targetParameter().targetName(), dataset, false);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    static public XYSeries createSeries(String archName, TargetParameter target, List<Double> metricValue) {
        var series = new XYSeries(archName);
        IntStream.range(0, Math.min(metricValue.size(), target.values().size())).forEach(i ->
                series.add(target.values().get(i), round(metricValue.get(i)))
        );
        return series;
    }

    static private Double round(Double value) {
        return Math.round(value * 10.0) / 10.0;
    }
}
