package statistic;

import client.parameters.ClientParameter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import metrics.TargetParameter;
import metrics.factory.MetricFactory;
import parameters.Parameters;
import utils.Constants;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class StatisticIO {
    public static class OutJsonFormat {
        public String arch;
        public HashMap<String, Integer> input = new HashMap<>();
        public TargetParameter target;
        public HashMap<String, List<Double>> metrics = new HashMap<>();
    }

    static public OutJsonFormat save(StatisticInfo statisticInfo) throws IOException {

        File outFile = Constants.statPath.resolve(statisticInfo.archType() + "_" + statisticInfo.targetParameter().targetName() + "_" + System.currentTimeMillis() + ".json").toFile();
        try (FileWriter out = new FileWriter(outFile)) {
            ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

            OutJsonFormat outFmt = new OutJsonFormat();
            outFmt.arch = statisticInfo.archType();
            statisticInfo.clientParameters().forEach(param -> {
                outFmt.input.put(param.parameterName().toString(), param.parameterValue());
            });
            outFmt.target = statisticInfo.targetParameter();
            statisticInfo.metrics().forEach(it -> outFmt.metrics.put(it.getMetricName(), it.metricValues));

            mapper.writeValue(out, outFmt);
            System.out.println(mapper.writeValueAsString(load(outFile)));
            return outFmt;
        }
    }

    public static StatisticInfo load(File statFile) throws IOException {
        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        var outFmt = mapper.reader().createParser(statFile).readValueAs(OutJsonFormat.class);
        return new StatisticInfo(outFmt.target, outFmt.arch,
                outFmt.input.entrySet().stream().map(it -> new ClientParameter(Parameters.fromString(it.getKey()), it.getValue())).toList(),
                outFmt.metrics.entrySet().stream().map(it -> MetricFactory.getMetric(it.getKey(), it.getValue())).toList());
    }
}
