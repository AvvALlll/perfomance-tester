package statistic;

import client.parameters.ClientParameter;
import metrics.Metric;
import metrics.TargetParameter;

import java.util.List;

public record StatisticInfo(TargetParameter targetParameter, String archType, List<ClientParameter> clientParameters, List<Metric> metrics) {
}
