plugins {
    id("java")
    application
    id("com.google.protobuf") version "0.8.19" apply false
}

group = "org.itmo.mse"
version = "1.0"

repositories {
    mavenCentral()
}

subprojects {
    apply {
        plugin("java")
    }
    repositories {
        mavenCentral()
    }

    dependencies {
        implementation("com.google.protobuf:protobuf-java:3.19.6")
    }
}

application {
    mainClass = "Runner"
}

dependencies {
    implementation("commons-cli:commons-cli:1.6.0")
    implementation(project(":common"))
    implementation(project(":measurement"))
    implementation(project(":dataset"))
    implementation(project(":ClientArch"))
    implementation(project(":AsyncArch"))
    implementation(project(":SyncBlockingArch"))
    implementation(project(":SyncNonBlockingArch"))
}


tasks.jar {
    manifest.attributes["Main-Class"] = application.mainClass
    val dependencies = configurations
        .runtimeClasspath
        .get()
        .map(::zipTree)
    from(dependencies)
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}
