package client;

import utils.Constants;

import java.nio.ByteBuffer;

public class ClientMemContext {
    public final ByteBuffer buffer = ByteBuffer.allocate(Constants.BUFFER_SIZE);
    public byte[] byteArray = new byte[0];
}
