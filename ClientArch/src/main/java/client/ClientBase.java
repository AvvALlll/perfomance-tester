package client;

import client.parameters.ClientParameter;

import java.io.IOException;
import java.util.List;

public interface ClientBase {

    void connect() throws IOException;
    void start(List<ClientParameter> clientParameters) throws IOException, ClassNotFoundException, InterruptedException;

    Double getAverageRequest();
}
