package client.parameters;

import parameters.Parameters;

public record ClientParameter(Parameters parameterName, Integer parameterValue) implements ClientParameterBase {
    @Override
    public boolean isRange() {
        return false;
    }
}
