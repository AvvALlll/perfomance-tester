package client.parameters;

import parameters.Parameters;

import java.util.HashMap;

public class ClientSettings {
    HashMap<Parameters, Integer> parameters = new HashMap<>();

    public ClientSettings() {
    }

   public void setParameter(Parameters parameterName, Integer value) {
        parameters.put(parameterName, value);
   }

   public Integer getParameter(Parameters parameterName) {
        return parameters.get(parameterName);
   }
}
