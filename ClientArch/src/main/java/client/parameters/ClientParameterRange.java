package client.parameters;

import parameters.Parameters;

public record ClientParameterRange(Parameters parameterName, Integer valueFrom, Integer valueTo,
                                   Integer step) implements ClientParameterBase {
    @Override
    public boolean isRange() {
        return true;
    }
}
