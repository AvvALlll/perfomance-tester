package client;


import client.parameters.ClientParameter;
import client.parameters.ClientSettings;
import network.NetworkService;
import request.RequestHandler;

import java.io.IOException;
import java.util.List;

public class Client implements ClientBase {
    RequestHandler requestHandler;

    public Client(ClientMemContext memContext) throws IOException {
        requestHandler = new RequestHandler(new NetworkService(memContext));
    }
    @Override
    public void connect() throws IOException {
        requestHandler.connect();
    }


    @Override
    public void start(List<ClientParameter> clientParameters) throws IOException, ClassNotFoundException, InterruptedException {
        ClientSettings clientSettings = new ClientSettings();
        clientParameters.forEach((par) -> clientSettings.setParameter(par.parameterName(), par.parameterValue()));
        requestHandler.requestProcessing(clientSettings);
    }

    @Override
    public Double getAverageRequest() {
        return requestHandler.getAverageRequest();
    }
}
