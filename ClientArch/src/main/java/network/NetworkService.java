package network;

import client.ClientMemContext;
import proto.MessageArray;
import utils.Constants;

import java.io.IOException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

import static java.lang.Integer.min;

public class NetworkService implements NetworkServiceBase {
    private ClientMemContext memContext;
    private SocketChannel socketChannel;

    public NetworkService(ClientMemContext memContext) throws IOException {
        this.memContext = memContext;
    }


    @Override
    public void connect() throws IOException {
        while (true) {
            try {
                socketChannel = SocketChannel.open();
                socketChannel.connect(new InetSocketAddress("localhost", Constants.SERVER_PORT));
                return;
            } catch (ConnectException e) {
                socketChannel.close();
            }
        }
    }

    @Override
    public void sendData(byte[] data) throws IOException {
        NetworkHelper.writeFullMessage(socketChannel, memContext.buffer, data);
    }

    @Override
    public MessageArray readData() throws IOException {
        var readRes = NetworkHelper.readFullMessage(socketChannel, memContext.buffer, memContext.byteArray);
        if (readRes == null) {
            throw new IOException("cannot read data");
        }
        memContext.byteArray = readRes.getKey();
        return MessageArray.parser().parseFrom(memContext.byteArray, 0, readRes.getValue());
    }

    @Override
    public void close() throws IOException {
        socketChannel.close();
    }
}
