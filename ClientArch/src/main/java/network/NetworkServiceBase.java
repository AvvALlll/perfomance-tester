package network;

import proto.MessageArray;

import java.io.IOException;

public interface NetworkServiceBase {

    void connect() throws IOException;

    void sendData(byte[] data) throws IOException;

    MessageArray readData() throws IOException;

    void close() throws IOException;
}
