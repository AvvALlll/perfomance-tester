package request;

import client.ClientMemContext;
import client.parameters.ClientSettings;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import measurement.ClientMeasurement;
import network.NetworkService;
import parameters.Parameters;
import proto.MessageArray;
import utils.ArrayUtils;

import java.io.IOException;
import java.util.Collections;

public class RequestHandler {
    ClientMeasurement clientMeasurement = new ClientMeasurement();
    NetworkService networkService;

    public RequestHandler(NetworkService networkService) throws IOException {
        this.networkService = networkService;
    }

    public void connect() throws IOException {
        networkService.connect();
    }
    public void requestProcessing(ClientSettings clientSettings) throws IOException, InterruptedException {
        var arraySize = clientSettings.getParameter(Parameters.ARRAY_SIZE);
        for (int i = 0; i < clientSettings.getParameter(Parameters.REQUEST_COUNT); ++i) {
            clientMeasurement.startMeasure();
            var array = ArrayUtils.generateArray(arraySize);
            MessageArray msg = MessageArray.newBuilder()
                    .setArraySize(arraySize).addAllData(
                            array
                    ).build();
            networkService.sendData(msg.toByteArray());
            var messageArray = networkService.readData();
            var sortedArray = messageArray.getDataList();
            Collections.sort(array);
            if (!sortedArray.equals(array)) {
                System.out.println("false");
            }
            clientMeasurement.stopMeasure();

            Thread.sleep(clientSettings.getParameter(Parameters.DELTA));
        }
        networkService.close();
    }

    public Double getAverageRequest() {
        return clientMeasurement.getAverage();
    }

}
