package server;

import measurement.ServerMeasurement;
import utils.Constants;

import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

public class ClientContext {
    ByteBuffer readBuffer = ByteBuffer.allocate(Constants.BUFFER_SIZE);
    ByteBuffer writeBuffer = ByteBuffer.allocate(Constants.BUFFER_SIZE);
    SocketChannel socketChannel;

    public final ExecutorService writeThread = Executors.newSingleThreadExecutor();

    public ClientContext(SocketChannel socketChannel) {
        this.socketChannel = socketChannel;
    }
}
