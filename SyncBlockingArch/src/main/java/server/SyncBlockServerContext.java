package server;

import com.google.protobuf.InvalidProtocolBufferException;
import measurement.ServerMeasurement;
import metrics.ServerMetrics;
import metrics.collector.MetricsCollector;
import metrics.collector.MetricsCollectorBase;
import network.NetworkHelper;
import proto.MessageArray;
import utils.Constants;
import utils.Sorting;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import static java.lang.Integer.min;

public class SyncBlockServerContext {
    MetricsCollectorBase metricsCollector;
    ThreadPoolExecutor threadPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(Constants.THREAD_POOL_SIZE);
    HashMap<ClientContext, Thread> clientContexts = new HashMap<>();

    SyncBlockServerContext(MetricsCollectorBase metricsCollector) {
        this.metricsCollector = metricsCollector;
    }

    void processMessage(AtomicBoolean isRunning, ClientContext clientContext) {
        var clientThread = new Thread(() -> {
            readData(isRunning, clientContext);
        });
        clientContexts.put(clientContext, clientThread);
        clientThread.start();
    }

    public void close() {
        metricsCollector.stopCollecting();
        threadPool.shutdown();
        clientContexts.forEach((key, value) -> {
            try {
                key.writeThread.shutdown();
                value.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public void resetMeasurements() {
        metricsCollector.resetMetrics();
        clientContexts.forEach((key, value) -> {
            try {
                key.writeThread.shutdown();
                value.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        clientContexts.clear();
    }

    private void readData(AtomicBoolean isRunning, ClientContext clientContext) {
        byte[] byteArray = new byte[0];
        while (isRunning.get()) {
            try {
                var readRes = NetworkHelper.readFullMessage(clientContext.socketChannel, clientContext.readBuffer, byteArray);
                if (readRes == null) {
                    metricsCollector.stopCollecting();
                    break;
                }
                byteArray = readRes.getKey();

                ServerMeasurement clientTime = new ServerMeasurement();
                metricsCollector.startMeasureTime(clientTime);
                handleMessage(byteArray, clientContext, clientTime);
            } catch (IOException e) {
                metricsCollector.stopCollecting();
                throw new RuntimeException(e);
            }
        }
    }

    private void handleMessage(byte[] data, ClientContext clientContext, ServerMeasurement clientRequestTime) {
        threadPool.submit(() -> {
            ArrayList<Integer> array;
            try {
                array = new ArrayList<Integer>(MessageArray.parseFrom(data).getDataList());
            } catch (InvalidProtocolBufferException e) {
                metricsCollector.stopCollecting();
                throw new RuntimeException(e);
            }

            var requestTime = new ServerMeasurement();
            metricsCollector.startMeasureTime(requestTime);
            Sorting.sort(array);
            metricsCollector.collectTotalRequestHandlingTime(requestTime);

            byte[] outData = MessageArray.newBuilder().setArraySize(array.size()).addAllData(array).build().toByteArray();

            metricsCollector.collectTotalClientHandlingTime(clientRequestTime);
            clientContext.writeThread.submit(() -> {
                try {
                    NetworkHelper.writeFullMessage(clientContext.socketChannel, clientContext.writeBuffer, outData);
                } catch (IOException e) {
                    metricsCollector.stopCollecting();
                    throw new RuntimeException(e);
                }
            });
        });
    }


}
