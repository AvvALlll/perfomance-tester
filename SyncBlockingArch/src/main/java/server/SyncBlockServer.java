package server;

import arch.ArchTypes;
import arch.ServerBase;
import metrics.collector.MetricsCollector;
import metrics.collector.MetricsCollectorAvg;
import metrics.collector.MetricsCollectorBase;
import utils.Constants;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.ServerSocketChannel;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;

public class SyncBlockServer implements ServerBase {
    MetricsCollectorBase metricsCollector = new MetricsCollector();
    SyncBlockServerContext context = new SyncBlockServerContext(metricsCollector);
    ServerSocketChannel serverSocketChannel;

    @Override
    public ArchTypes getArchType() {
        return ArchTypes.SYNC_BLOCK;
    }

    @Override
    public void start(AtomicBoolean isRunning) throws IOException {
        serverSocketChannel = ServerSocketChannel.open().bind(new InetSocketAddress(Constants.SERVER_PORT), 200);
        while (isRunning.get()) {
            ClientContext clientContext;
            try {
                var clientSocket = serverSocketChannel.accept();
                clientContext = new ClientContext(clientSocket);
            } catch (AsynchronousCloseException e) {
                if (!isRunning.get())
                    break;
                throw new RuntimeException("unexpected server socket close");
            }
            context.processMessage(isRunning, clientContext);
        }
        context.close();

    }

    @Override
    public void stop() throws IOException {
        serverSocketChannel.close();
    }

    @Override
    public void reset() {
        context.resetMeasurements();
    }

    @Override
    public metrics.ServerMetrics getMetrics() {
        return metricsCollector.calculateMetrics();
    }
}
