import com.google.protobuf.gradle.*

apply {
    plugin("com.google.protobuf")
}

group = "org.itmo.mse"
version = "1.0"

sourceSets {
    main {
        proto {
            srcDir("src/proto")
        }
        java {
            srcDir("build/generated/source/proto/main/java/")
        }
    }
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:3.19.6"
    }
}


dependencies {
    implementation(project(":measurement"))
}