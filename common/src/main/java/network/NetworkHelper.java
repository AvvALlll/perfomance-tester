package network;

import utils.Constants;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.AbstractMap;
import java.util.Map;

import static java.lang.Integer.min;

public class NetworkHelper {

    static public Map.Entry<byte[], Integer> readFullMessage(SocketChannel socketChannel, ByteBuffer readBuffer, byte[] outData) throws IOException {
        readBuffer.clear();
        var readSize = socketChannel.read(readBuffer);
        if (readSize < 0)
            return null;

        readSize -= Integer.BYTES;
        readBuffer.flip();
        int totalMessageSize = readBuffer.getInt(), curMessageSize = totalMessageSize;
        readBuffer.compact();

        outData = new byte[totalMessageSize];

        while (true) {
            readBuffer.flip();
            readBuffer.get(outData, totalMessageSize - curMessageSize, readSize);
            curMessageSize -= readSize;
            if (curMessageSize == 0) {
                break;
            }

            readBuffer.clear();
            readSize = socketChannel.read(readBuffer);
        }
        return Map.entry(outData, totalMessageSize);
    }

    static public void writeFullMessage(SocketChannel socketChannel, ByteBuffer writeBuffer, byte[] inData) throws IOException {
        writeBuffer.clear();
        var dataSize = inData.length;
        var intSize = Integer.BYTES;
        writeBuffer.putInt(dataSize);
        while (dataSize > 0) {
            writeBuffer.put(inData, inData.length - dataSize, min(dataSize, Constants.BUFFER_SIZE - intSize));
            dataSize = dataSize + intSize - Constants.BUFFER_SIZE;
            intSize = 0;
            writeBuffer.flip();
            while (writeBuffer.hasRemaining()) {
                socketChannel.write(writeBuffer);
            }
            writeBuffer.clear();
        }
    }
}
