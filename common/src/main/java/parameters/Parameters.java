package parameters;

public enum Parameters {
    CLIENTS_COUNT,
    ARRAY_SIZE,
    REQUEST_COUNT,
    DELTA;

    @Override
    public String toString() {
        return switch (this) {
            case CLIENTS_COUNT -> "M";
            case ARRAY_SIZE -> "N";
            case REQUEST_COUNT -> "X";
            case DELTA -> "delta";
        };
    }

    public static Parameters fromString(String str) {
        return switch (str) {
            case "M" -> CLIENTS_COUNT;
            case "N" -> ARRAY_SIZE;
            case "X" -> REQUEST_COUNT;
            case "delta" -> DELTA;
            default -> throw new RuntimeException("unrecognized parameter name");
        };
    }
}
