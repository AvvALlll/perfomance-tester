package utils;

import java.nio.file.Path;

public class Constants {
    public static final Path graphicPath = Path.of("graphics");
    public static final Path statPath = Path.of("stat");

    static  {
        if (!graphicPath.toFile().exists())
            graphicPath.toFile().mkdir();
        if (!statPath.toFile().exists())
            statPath.toFile().mkdir();
    }
    public static final int SKIP_RANGE = 4;
    public static final int SERVER_PORT = 8081;
    public static final int BUFFER_SIZE = 1024 * 16;
    public static final int THREAD_POOL_SIZE = 16;
}
