package utils;

import com.google.protobuf.ByteString;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class ArrayUtils {

    public static ArrayList<Integer> generateArray(Integer arraySize) {
        var array = new ArrayList<Integer>(arraySize);
        Random rand = new Random();
        rand.setSeed(System.currentTimeMillis());
        for (int i = 0; i < arraySize; ++i) {
            array.add(rand.nextInt());
        }

        return array;
    }


    public static byte[] convertToByteArray(ArrayList<Integer> array) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        ObjectOutputStream objStream = new ObjectOutputStream(output);
        objStream.writeObject(array);
        return output.toByteArray();
    }

    public static ArrayList<Integer> convertToArrayList(byte[] byteArray) throws IOException, ClassNotFoundException {

        ByteArrayInputStream bais = new ByteArrayInputStream(byteArray);
        ObjectInputStream objStream = new ObjectInputStream(bais);
        var list = (ArrayList<Integer>) objStream.readObject();

        return list;
    }
}
