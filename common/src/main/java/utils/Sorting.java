package utils;

import java.util.ArrayList;
import java.util.List;

public class Sorting {
    public static <E extends Comparable<E>>void sort(List<E> array) {
        for (int i = 0; i < array.size(); ++i) {
            for (int j = 0; j < array.size(); ++j) {
                if (array.get(j).compareTo(array.get(i)) > 0) {
                    var tmp = array.get(j);
                    var iValue = array.get(i);
                    array.set(j, iValue);
                    array.set(i, tmp);
                }
            }
        }
    }
}
