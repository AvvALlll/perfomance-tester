package arch;

import metrics.ServerMetrics;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;

public interface ServerBase {
    ArchTypes getArchType();
    void start(AtomicBoolean isRunning) throws ExecutionException, InterruptedException, IOException;

    void stop() throws IOException;

    void reset();

    ServerMetrics getMetrics();
}
