package arch;

public enum ArchTypes {
    ASYNC,
    SYNC_BLOCK,
    SYNC_NONBLOCK;

    @Override
    public String toString() {
        return switch (this) {
            case ASYNC -> "async";
            case SYNC_BLOCK -> "syncBlock";
            case SYNC_NONBLOCK -> "syncNonBlock";
        };
    }
}
